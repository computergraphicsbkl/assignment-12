from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import arange
from PIL.Image import open

width = 600
height = 600

camera_x_rotation = 0
camera_y_rotation = 0
camera_angle_delta = 5

color = (1, 0, 0, 1)
strip_width = 10
strip_mode = 1

program = None
uniform_color = None



def init():
    global program_plain, uniform_plain_color
    global program_stripped, uniform_stripped_color, uniform_stripped_width, uniform_stripped_mode

    glClearColor(0, 0.6, 1, 1)
    glEnable(GL_DEPTH_TEST)
    reshape(width, height)

    fragment_plain = create_shader(GL_FRAGMENT_SHADER, """
uniform vec4 color;
void main()
{
    gl_FragColor = color;
}
        """)
    fragment_stripped = create_shader(GL_FRAGMENT_SHADER, """
uniform vec4 color;
uniform float width;
uniform int mode;
void main()
{
    if (mode == 0 && mod(gl_FragCoord.y, 2. * width) > width
        || mode == 1 && mod(gl_FragCoord.x, 2. * width) > width)
        gl_FragColor = color;
    else
        gl_FragColor = vec4(1. - color.r, 1. - color.g, 1. - color.b, 1);
}
        """)

    program_plain = glCreateProgram()
    program_stripped = glCreateProgram()
    glAttachShader(program_plain, fragment_plain)
    glAttachShader(program_stripped, fragment_stripped)
    glLinkProgram(program_plain)
    glLinkProgram(program_stripped)
    uniform_plain_color = glGetUniformLocation(program_plain, 'color')
    uniform_stripped_color = glGetUniformLocation(program_stripped, 'color')
    uniform_stripped_width = glGetUniformLocation(program_stripped, 'width')
    uniform_stripped_mode = glGetUniformLocation(program_stripped, 'mode')

def update():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    handle_camera()

    glUseProgram(program_plain)
    glTranslate(-1, 0, 0)
    glUniform4fv(uniform_plain_color, 1, color)
    glutSolidCube(1)

    glUseProgram(program_stripped)
    glTranslate(2, 0, 0)
    glUniform4fv(uniform_stripped_color, 1, color)
    glUniform1f(uniform_stripped_width, strip_width)
    glUniform1i(uniform_stripped_mode, strip_mode)
    glutSolidCube(1)

    glUseProgram(0)

    glutSwapBuffers()


def reshape(new_width, new_height):
    global width, height

    width = new_width
    height = new_height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, width / height, 0.1, 100)


def special(key, x, y):
    global camera_x_rotation, camera_y_rotation

    if key == GLUT_KEY_UP:
        camera_x_rotation += camera_angle_delta
    if key == GLUT_KEY_DOWN:
        camera_x_rotation -= camera_angle_delta
    if key == GLUT_KEY_LEFT:
        camera_y_rotation += camera_angle_delta
    if key == GLUT_KEY_RIGHT:
        camera_y_rotation -= camera_angle_delta


def create_shader(shader_type, source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, source)
    glCompileShader(shader)
    return shader


def handle_camera():
    glTranslate(0, 0, -5)
    glRotate(camera_x_rotation, 1, 0, 0)
    glRotate(camera_y_rotation, 0, 1, 0)


def main():
    glutInit()
    glutInitWindowSize(width, height)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    glutCreateWindow('OpenGL')
    glutIdleFunc(update)
    glutDisplayFunc(update)
    glutReshapeFunc(reshape)
    glutSpecialFunc(special)
    init()
    glutMainLoop()


if __name__ == '__main__':
    main()
