﻿//#define GLEW_STATIC
//
//#include <iostream>
//#include <sstream>
//#include <fstream>
//#include <string>
//#include <GL/glew.h>
//#include <GL/freeglut.h>
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
//#include "SOIL.h"
//
//const unsigned long FOURCC_DXT1 = 0x31545844; //(MAKEFOURCC('D','X','T','1'))
//const unsigned long FOURCC_DXT3 = 0x33545844; //(MAKEFOURCC('D','X','T','3'))
//const unsigned long FOURCC_DXT5 = 0x35545844; //(MAKEFOURCC('D','X','T','5'))
//
//
//using namespace std;
//
//char *shaderPath = "shader.glsl";
////! Переменные с индентификаторами ID
////! ID шейдерной программы
//GLuint Program;
////! ID атрибута
//GLint Attrib_vertex;
////! ID юниформ переменной цвета
//GLint Unif_color;
//GLint Unif_angle;
//GLint Unif_rotationX;
//GLint Unif_rotationY;
//GLint Unif_rotationZ;
//GLint Unif_scale;
//float rotate_x = 0;
//float rotate_y = 0;
//float rotate_z = 0;
//float scale_x = 1;
//float scale_y = 1;
//float scale_z = 1;
//
////! Проверка ошибок OpenGL, если есть то вывод в консоль тип ошибки
//void checkOpenGLerror()
//{
//	GLenum errCode;
//	if ((errCode = glGetError()) != GL_NO_ERROR)
//		std::cout << "OpenGl error! - " << gluErrorString(errCode);
//}
//
//char* readFile(char* path) {
//	ifstream fp;
//	fp.open(path, ios_base::in);
//	string buffer;
//	if (fp) {
//		string line;
//		while (getline(fp, line)) {
//			buffer.append(line);
//			buffer.append("\r\n");
//		}
//	}
//	else {
//		cerr << "Error loading shader: " << path << endl;
//	}
//	char *result = new char[buffer.length() + 1];
//	strcpy(result, buffer.c_str());
//	return result;
//}
//
//static void checkScaleBounds() {
//	if (scale_x < 0.1f) scale_x = 0.1f;
//	if (scale_y < 0.1f) scale_y = 0.1f;
//	if (scale_z < 0.1f) scale_z = 0.1f;
//}
//
////! Инициализация шейдеров
//void initShader()
//{
//
//	std::cout << "B" << std::endl;
//	checkOpenGLerror();
//
//
//	//vertexShaderStuff()
//
//	//fragmentShaderStuff();
//
//	const char* vsSource = readFile("vertexShader.txt");
//	const char* fsSource = readFile("fragmentShader.txt");
//
//	std::cout << "1" << std::endl;
//	checkOpenGLerror();
//
//
//	//! Переменные для хранения идентификаторов шейдеров
//	GLuint vShader, fShader;
//
//	std::cout << "2" << std::endl;
//	checkOpenGLerror();
//
//
//	//! Создаем вершинный шейдер
//	vShader = glCreateShader(GL_VERTEX_SHADER);
//	//! Передаем исходный код
//	glShaderSource(vShader, 1, &vsSource, NULL);
//	//! Компилируем шейдер
//	glCompileShader(vShader);
//
//	std::cout << "3" << std::endl;
//	checkOpenGLerror();
//
//
//
//	//! Создаем фрагментный шейдер
//	fShader = glCreateShader(GL_FRAGMENT_SHADER);
//	//! Передаем исходный код
//	glShaderSource(fShader, 1, &fsSource, NULL);
//	//! Компилируем шейдер
//	glCompileShader(fShader);
//
//	std::cout << "4" << std::endl;
//	checkOpenGLerror();
//
//	//! Создаем программу и прикрепляем шейдеры к ней
//	Program = glCreateProgram();
//
//	//std::cout << Program << " " << vShader << " " << fShader << std::endl;
//	checkOpenGLerror();
//
//	//glAttachShader(Program, vShader);
//	//glAttachShader(Program, fShader);
//
//	std::cout << "5" << std::endl;
//	checkOpenGLerror();
//
//	//! Линкуем шейдерную программу
//	//glLinkProgram(Program);
//
//	std::cout << "6" << std::endl;
//	checkOpenGLerror();
//
//	//! Проверяем статус сборки
//	int link_ok=0;
//	//glGetProgramiv(Program, GL_LINK_STATUS, &link_ok);
//	if (!link_ok)
//	{
//		std::cout << "error attach shaders \n";
//		return;
//	}
//
//	////! Вытягиваем ID юниформ
//	char* unif_name = "rotationX";
//	//Unif_rotationX = glGetUniformLocation(Program, unif_name);
//	if (Unif_rotationX == -1)
//	{
//		std::cout << "could not bind uniform " << unif_name << std::endl;
//		return;
//	}
//
//	std::cout << "7" << std::endl;
//	checkOpenGLerror();
//
//	unif_name = "rotationY";
//	//Unif_rotationY = glGetUniformLocation(Program, unif_name);
//	if (Unif_rotationY == -1)
//	{
//		std::cout << "could not bind uniform " << unif_name << std::endl;
//		return;
//	}
//
//	std::cout << "8" << std::endl;
//	checkOpenGLerror();
//
//	unif_name = "rotationZ";
//	//Unif_rotationZ = glGetUniformLocation(Program, unif_name);
//	if (Unif_rotationZ == -1)
//	{
//		std::cout << "could not bind uniform " << unif_name << std::endl;
//		return;
//	}
//
//	std::cout << "9" << std::endl;
//	checkOpenGLerror();
//
//	unif_name = "scale";
//	//Unif_scale = glGetUniformLocation(Program, unif_name);
//	if (Unif_scale == -1)
//	{
//		std::cout << "could not bind uniform " << unif_name << std::endl;
//		return;
//	}
//
//	checkOpenGLerror();
//}
//
////! Освобождение шейдеров
//void freeShader()
//{
//	//! Передавая ноль, мы отключаем шейдрную программу
//	glUseProgram(0);
//	//! Удаляем шейдерную программу
//	glDeleteProgram(Program);
//}
//
//void resizeWindow(int width, int height)
//{
//	glViewport(0, 0, width, height);
//}
//
//void textureStuff() {
//
//	GLuint textureID = SOIL_load_OGL_texture // load an image file directly as a new OpenGL texture
//	(
//		"field_128_cube.dds",
//		SOIL_LOAD_AUTO,
//		SOIL_CREATE_NEW_ID,
//		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
//	);
//	// allocate a texture name
//
//	glBindTexture(GL_TEXTURE_2D, textureID);
//}
//
////! Отрисовка
//void render1()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	glLoadIdentity();
//
//	//! Устанавливаем шейдерную программу текущей
//	glUseProgram(Program);
//	
//	float angleX = 3.14f * rotate_x / 180.0f;
//	float rotateX[] = { 
//		1, 0, 0, 0,
//		0, cos(angleX), sin(angleX), 0,
//		0, -sin(angleX), cos(angleX), 0,
//		0, 0, 0, 1 };
//	//glUniformMatrix4fv(Unif_rotationX, 1, false, rotateX);
//
//	float angleY = 3.14f * rotate_y / 180.0f;
//	float rotateY[] = { 
//		cos(angleY), 0, -sin(angleY), 0,
//		0, 1, 0, 0,
//		sin(angleY), 0, cos(angleY), 0,
//		0, 0, 0, 1 };
//	//glUniformMatrix4fv(Unif_rotationY, 1, false, rotateY);
//
//	float angleZ = 3.14f * rotate_z / 180.0f;
//	float rotateZ[] = { 
//		cos(angleZ), sin(angleZ), 0, 0,
//		-sin(angleZ), cos(angleZ), 0, 0,
//		0, 0, 1, 0,
//		0, 0, 0, 1 };
//	//glUniformMatrix4fv(Unif_rotationZ, 1, false, rotateZ);
//
//	checkScaleBounds();
//	float scale[] = {
//		scale_x, 0, 0, 0,
//		0, scale_y, 0, 0,
//		0, 0, scale_z, 0,
//		0, 0, 0, 1 };
//	//glUniformMatrix4fv(Unif_scale, 1, false, scale);
//
//	GLuint textureID = SOIL_load_OGL_texture // load an image file directly as a new OpenGL texture
//	(
//		"field_128_cube.dds",
//		SOIL_LOAD_AUTO,
//		SOIL_CREATE_NEW_ID,
//		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
//	);
//	// allocate a texture name
//
//	glBindTexture(GL_TEXTURE_2D, textureID);
//
//	//glColor3f(0.804, 0.498, 0.196);
//	glutSolidCube(0.5);
//	//glColor3f(0.643, 0.40, 0.157);
//	glutWireCube(0.5);
//
//	glFlush();
//	//! Отключаем шейдерную программу
//	glUseProgram(GL_ZERO);
//	checkOpenGLerror();
//	glutSwapBuffers();
//}
//
//void specialKeys(int key, int x, int y) {
//	float angle;
//	switch (key) {
//	case GLUT_KEY_UP: rotate_x += 5; break;
//	case GLUT_KEY_DOWN: rotate_x -= 5; break;
//	case GLUT_KEY_RIGHT: rotate_y += 5; break;
//	case GLUT_KEY_LEFT: rotate_y -= 5; break;
//	case GLUT_KEY_PAGE_UP: rotate_z += 5; break;
//	case GLUT_KEY_PAGE_DOWN: rotate_z -= 5; break;
//	case GLUT_KEY_F1: scale_x += 0.1; break;
//	case GLUT_KEY_F2: scale_y += 0.1; break;
//	case GLUT_KEY_F3: scale_z += 0.1; break;
//	case GLUT_KEY_F5: scale_x -= 0.1; break;
//	case GLUT_KEY_F6: scale_y -= 0.1; break;
//	case GLUT_KEY_F7: scale_z -= 0.1; break;
//	case GLUT_KEY_F9: scale_x += 0.1; scale_y += 0.1; scale_z += 0.1; break;
//	case GLUT_KEY_F11: scale_x -= 0.1; scale_y -= 0.1; scale_z -= 0.1; break;
//	}
//	glutPostRedisplay();
//}
//
//int main(int argc, char **argv)
//{
//	setlocale(LC_ALL, "Rus");
//	glutInit(&argc, argv);
//	glutInitWindowPosition(100, 100);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);
//	glutInitWindowSize(800, 800);
//	glutCreateWindow("Simple shaders");
//	glClearColor(0, 0, 1, 0);
//	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_TEXTURE_2D);
//
//	//! Обязательно перед инициализацией шейдеров
//	GLenum glew_status = glewInit();
//	if (GLEW_OK != glew_status)
//	{
//		//! GLEW не проинициализировалась
//		std::cout << "Error: " << glewGetErrorString(glew_status) << "\n";
//		return 1;
//	}
//	//! Проверяем доступность OpenGL 2.0
//	if (!GLEW_VERSION_2_0)
//	{
//		//! OpenGl 2.0 оказалась не доступна
//		std::cout << "No support for OpenGL 2.0 found\n";
//		return 1;
//	}
//	//! Инициализация шейдеров
//	std::cout << "A" << std::endl;
//
//	initShader();
//	glutReshapeFunc(resizeWindow);
//	glutDisplayFunc(render1);
//	//glutSpecialFunc(specialKeys);
//	glutMainLoop();
//	//! Освобождение ресурсов
//	freeShader();
//}