from OpenGL.GL import *
from OpenGL.GL import shaders
from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import arange
from PIL.Image import open

width = 600
height = 600
camera_x_rotation = 0
camera_y_rotation = 0
camera_angle_delta = 5

program = None


def init():
    global program

    glClearColor(0, 0.6, 1, 1)
    glEnable(GL_DEPTH_TEST)
    reshape(width, height)

    vertex = shaders.compileShader("""
#version 120
void main()
{
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
        """, GL_VERTEX_SHADER);
#     vertex = create_shader(GL_VERTEX_SHADER, """
# void main()
# {
#     gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
# }
#         """)

    fragment = create_shader(GL_FRAGMENT_SHADER, """
void main()
{

}
        """)

    program = glCreateProgram()
    #glAttachShader(program, vertex)
    glAttachShader(program, fragment)
    glLinkProgram(program)


def update():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    handle_camera()

    #glUseProgram(program)

    glutSolidCube(1)

    glUseProgram(0)

    glutSwapBuffers()


def reshape(new_width, new_height):
    global width, height

    width = new_width
    height = new_height
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, width / height, 0.1, 100)


def special(key, x, y):
    global camera_x_rotation, camera_y_rotation

    if key == GLUT_KEY_UP:
        camera_x_rotation += camera_angle_delta
    if key == GLUT_KEY_DOWN:
        camera_x_rotation -= camera_angle_delta
    if key == GLUT_KEY_LEFT:
        camera_y_rotation += camera_angle_delta
    if key == GLUT_KEY_RIGHT:
        camera_y_rotation -= camera_angle_delta


def create_shader(shader_type, source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, source)
    glCompileShader(shader)
    return shader


def handle_camera():
    glTranslate(0, 0, -5)
    glRotate(camera_x_rotation, 1, 0, 0)
    glRotate(camera_y_rotation, 0, 1, 0)


def main():
    glutInit()
    glutInitWindowSize(width, height)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    glutCreateWindow('OpenGL')
    glutIdleFunc(update)
    glutDisplayFunc(update)
    glutReshapeFunc(reshape)
    glutSpecialFunc(special)
    init()
    glutMainLoop()


if __name__ == '__main__':
    main()
