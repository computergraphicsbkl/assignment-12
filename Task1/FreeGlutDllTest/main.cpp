﻿#define GLEW_STATIC

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

char *shaderPath = "shader.glsl";
//! Переменные с индентификаторами ID
//! ID шейдерной программы
GLuint Program;
//! ID атрибута
GLint Attrib_vertex;
//! ID юниформ переменной цвета
GLint Unif_color;
GLint Unif_angle;
GLint Unif_rotationX;
GLint Unif_rotationY;
GLint Unif_rotationZ;
GLint Unif_scale;
float rotate_x = 0;
float rotate_y = 0;
float rotate_z = 0;
float scale_x = 1;
float scale_y = 1;
float scale_z = 1;

//! Проверка ошибок OpenGL, если есть то вывод в консоль тип ошибки
void checkOpenGLerror()
{
	GLenum errCode;
	if ((errCode = glGetError()) != GL_NO_ERROR)
		std::cout << "OpenGl error! - " << gluErrorString(errCode);
}

char* readFile(char* path) {
	ifstream fp;
	fp.open(path, ios_base::in);
	string buffer;
	if (fp) {
		string line;
		while (getline(fp, line)) {
			buffer.append(line);
			buffer.append("\r\n");
		}
	}
	else {
		cerr << "Error loading shader: " << path << endl;
	}
	char *result = new char[buffer.length() + 1];
	strcpy(result, buffer.c_str());
	return result;
}

static void checkScaleBounds() {
	if (scale_x < 0.1f) scale_x = 0.1f;
	if (scale_y < 0.1f) scale_y = 0.1f;
	if (scale_z < 0.1f) scale_z = 0.1f;
}

//! Инициализация шейдеров
void initShader()
{
	//! Исходный код шейдеров
	char* vsSource = readFile(shaderPath);

	GLuint vShader;// , fShader;
	//! Создаем вершинный шейдер
	vShader = glCreateShader(GL_VERTEX_SHADER);
	//! Передаем исходный код
	glShaderSource(vShader, 1, &vsSource, NULL);
	//! Компилируем шейдер
	glCompileShader(vShader);
	//! Создаем программу и прикрепляем шейдеры к ней
	Program = glCreateProgram();
	glAttachShader(Program, vShader);
	//glAttachShader(Program, fShader);
	//! Линкуем шейдерную программу
	glLinkProgram(Program);
	//! Проверяем статус сборки
	int link_ok;
	glGetProgramiv(Program, GL_LINK_STATUS, &link_ok);
	if (!link_ok)
	{
		std::cout << "error attach shaders \n";
		return;
	}
	//! Вытягиваем ID атрибута из собранной программы
	const char* attr_name = "coord";
	Attrib_vertex = glGetAttribLocation(Program, attr_name);
	if (Attrib_vertex == -1)
	{
		std::cout << "could not bind attrib " << attr_name << std::endl;
		return;
	}
	////! Вытягиваем ID юниформ
	char *unif_name = "rotationX";
	Unif_rotationX = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationX == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "rotationY";
	Unif_rotationY = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationY == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "rotationZ";
	Unif_rotationZ = glGetUniformLocation(Program, unif_name);
	if (Unif_rotationZ == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	unif_name = "scale";
	Unif_scale = glGetUniformLocation(Program, unif_name);
	if (Unif_scale == -1)
	{
		std::cout << "could not bind uniform " << unif_name << std::endl;
		return;
	}
	checkOpenGLerror();
}

//! Освобождение шейдеров
void freeShader()
{
	//! Передавая ноль, мы отключаем шейдрную программу
	glUseProgram(0);
	//! Удаляем шейдерную программу
	glDeleteProgram(Program);
}

void resizeWindow(int width, int height)
{
	glViewport(0, 0, width, height);
}

//! Отрисовка
void render1()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//! Устанавливаем шейдерную программу текущей
	glUseProgram(Program);
	
	float angleX = 3.14f * rotate_x / 180.0f;
	float rotateX[] = { 
		1, 0, 0, 0,
		0, cos(angleX), sin(angleX), 0,
		0, -sin(angleX), cos(angleX), 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationX, 1, false, rotateX);

	float angleY = 3.14f * rotate_y / 180.0f;
	float rotateY[] = { 
		cos(angleY), 0, -sin(angleY), 0,
		0, 1, 0, 0,
		sin(angleY), 0, cos(angleY), 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationY, 1, false, rotateY);

	float angleZ = 3.14f * rotate_z / 180.0f;
	float rotateZ[] = { 
		cos(angleZ), sin(angleZ), 0, 0,
		-sin(angleZ), cos(angleZ), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_rotationZ, 1, false, rotateZ);

	checkScaleBounds();
	float scale[] = {
		scale_x, 0, 0, 0,
		0, scale_y, 0, 0,
		0, 0, scale_z, 0,
		0, 0, 0, 1 };
	glUniformMatrix4fv(Unif_scale, 1, false, scale);

	glBegin(GL_LINES);
	glVertex3f(-0.5f, -0.5f, 0);
	glVertex3f(-0.5f, 0.5f, 0);
	glVertex3f(-0.5f, 0.5f, 0);
	glVertex3f(0.5f, 0.5f, 0);
	glVertex3f(0.5f, 0.5f, 0);
	glVertex3f(0.5f, -0.5f, 0);
	glVertex3f(0.5f, -0.5f, 0);
	glVertex3f(-0.5f, -0.5f, 0);
	glEnd();
	glFlush();
	//! Отключаем шейдерную программу
	glUseProgram(GL_ZERO);
	checkOpenGLerror();
	glutSwapBuffers();
}

void specialKeys(int key, int x, int y) {
	float angle;
	switch (key) {
	case GLUT_KEY_UP: rotate_x += 5; break;
	case GLUT_KEY_DOWN: rotate_x -= 5; break;
	case GLUT_KEY_RIGHT: rotate_y += 5; break;
	case GLUT_KEY_LEFT: rotate_y -= 5; break;
	case GLUT_KEY_PAGE_UP: rotate_z += 5; break;
	case GLUT_KEY_PAGE_DOWN: rotate_z -= 5; break;
	case GLUT_KEY_F1: scale_x += 0.1; break;
	case GLUT_KEY_F2: scale_y += 0.1; break;
	case GLUT_KEY_F3: scale_z += 0.1; break;
	case GLUT_KEY_F5: scale_x -= 0.1; break;
	case GLUT_KEY_F6: scale_y -= 0.1; break;
	case GLUT_KEY_F7: scale_z -= 0.1; break;
	case GLUT_KEY_F9: scale_x += 0.1; scale_y += 0.1; scale_z += 0.1; break;
	case GLUT_KEY_F11: scale_x -= 0.1; scale_y -= 0.1; scale_z -= 0.1; break;
	}
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
	setlocale(LC_ALL, "Rus");
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Simple shaders");
	glClearColor(1, 1, 1, 0);
	glMatrixMode(GL_PROJECTION);
	//! Обязательно перед инициализацией шейдеров
	GLenum glew_status = glewInit();
	if (GLEW_OK != glew_status)
	{
		//! GLEW не проинициализировалась
		std::cout << "Error: " << glewGetErrorString(glew_status) << "\n";
		return 1;
	}
	//! Проверяем доступность OpenGL 2.0
	if (!GLEW_VERSION_2_0)
	{
		//! OpenGl 2.0 оказалась не доступна
		std::cout << "No support for OpenGL 2.0 found\n";
		return 1;
	}
	//! Инициализация шейдеров
	initShader();
	glutReshapeFunc(resizeWindow);
	glutDisplayFunc(render1);
	glutSpecialFunc(specialKeys);
	glutMainLoop();
	//! Освобождение ресурсов
	freeShader();
}